<?php
try {
    $mongo = new MongoDB\Driver\Manager("mongo://root:example@bdd_nosql_1:27017/fitness_db");

    $filter = [];
    $options = [];
    $query = new MongoDB\Driver\Query($filter, $options);
    $cursor = $mongo->executeQuery("fitness_db.fintness_data", $query);
    $result = [];
    foreach ($cursor as $document) {
        $infos_inventeur = [];
        $entry = json_decode(json_encode($document), true);
        unset($entry["_id"]);
        foreach ($entry as $key => $value){
            $infos_inventeur[$key] = $value;
        }
        array_push($result, $infos_inventeur);
    }
    return $result;
}
catch (Exception $e) {

}

echo '<div><ul>';
foreach($cursor as $line) {
    echo '<li>'.$line['date'].
    '</li>'.$line['step_count'].
    '<li>'.$line['mood'].
    '</li>'.$line['calories_burned'].
    '<li>'.$line['hours_of_sleep'].
    '</li>'.$line['bool_of_active'].
    '<li>'.$line['weight_kg'].
    '</li>';
}
echo '</ul></div>';
?>